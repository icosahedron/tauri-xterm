const { invoke } = window.__TAURI__.tauri;

const term = new Terminal({
  cursorBlink: true,
  macOptionIsMeta: true,
  scrollback: true,
  allowProposedApi: true,
});
term.attachCustomKeyEventHandler(customKeyEventHandler);
// https://github.com/xtermjs/xterm.js/issues/2941
const fit = new FitAddon.FitAddon();
term.loadAddon(fit);
const canvas = new CanvasAddon.CanvasAddon();
term.loadAddon(canvas);

term.open(document.getElementById("terminal"));
fit.fit();
term.resize(80, 50);
console.log(`size: ${term.cols} columns, ${term.rows} rows`);
fit.fit();
term.prevCols = term.cols;
term.prevRows = term.rows;
term.writeln("Welcome to xterm-tauri.js!");
term.writeln('')
// (async () => {
//   let response = await invoke('greet', { name: 'tauri' })
//   term.writeln(response);
// })();
invoke('greet', { name: 'tauri' })
  .then((response) => { term.writeln(response); });
async function greet() {
  let response = await invoke('greet', { name: 'tauri' })
  term.writeln(response);
}
greet();
var count = 0;
term.onData((data) => {
  console.log("browser terminal received new data:", data);
  // term.write(data);
  invoke('echo2',{ echo: { ch : data, count : count }})
    .then((response) => { 
      count = response.count;
      term.write(response.ch);
      console.log(response.count);
    });
});

function resizeTerminal() {
  const dims = fit.proposeDimensions();
  console.log("fit.proposeDimensions() = ", dims);
  if (dims.cols < 81 || dims.rows < 26) {
    console.log("terminal too small, enabling scrollbars", dims);
    term.resize(80, 25);
  }
  else {
    fit.fit();
    console.log("sending new dimensions to server's pty", dims);
  }
}

function debounce(func, wait_ms) {
  let timeout;
  return function (...args) {
    const context = this;
    clearTimeout(timeout);
    timeout = setTimeout(() => func.apply(context, args), wait_ms);
  };
}

/**
 * Handle copy and paste events
 */
function customKeyEventHandler(e) {
  if (e.type !== "keydown") {
    return true;
  }
  if (e.ctrlKey && e.shiftKey) {
    const key = e.key.toLowerCase();
    if (key === "v") {
      // ctrl+shift+v: paste whatever is in the clipboard
      navigator.clipboard.readText().then((toPaste) => {
        term.writeText(toPaste);
      });
      return false;
    } else if (key === "c" || key === "x") {
      // ctrl+shift+x: copy whatever is highlighted to clipboard

      // 'x' is used as an alternate to 'c' because ctrl+c is taken
      // by the terminal (SIGINT) and ctrl+shift+c is taken by the browser
      // (open devtools).
      // I'm not aware of ctrl+shift+x being used by anything in the terminal
      // or browser
      const toCopy = term.getSelection();
      navigator.clipboard.writeText(toCopy);
      term.focus();
      return false;
    }
  }
  return true;
}

const wait_ms = 200;
window.onresize = debounce(resizeTerminal, wait_ms);

