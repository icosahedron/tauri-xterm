#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use console::style;

// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
#[tauri::command]
fn greet(name: &str) -> String {
    format!("Hello, {}! You've been greeted from Rust!", name)
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct Echo {
    ch : String,
    count : i32,
}

#[tauri::command]
fn echo(ch: &str, _count : usize) -> String {
    ch.to_string()
}

#[tauri::command]
fn echo2(echo : Echo) -> Echo {
    Echo { count : echo.count + 1, ch : format!("{}", style(&echo.ch).green()) }
}

fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![greet, echo, echo2])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
